import secrets
from random import randint

from flask import Blueprint, render_template, flash, session, current_app, request, redirect

from app.logic.dag_transaction import get_transaction_log, read_transaction, set_transaction_log, write_transaction
from app.logic.db_transaction import create_transaction, select_transaction 
from app.utils.constants import vehicle_models_years, vehicle_brands, vehicle_types

Home = Blueprint('home', __name__)

APP_CONFIG = current_app.config
iota_api = APP_CONFIG['API_CLIENT']
iota_address = APP_CONFIG['ADDRESS']

@Home.route('/', methods=('GET', 'POST'))
def index():
    return render_template('index.html')


@Home.route('/registry', methods=['GET', 'POST'])
def registry():
    form = request.form.to_dict()
    method = request.method
    if method == 'POST' and form:
        return registry_transaction(form)
    else:
        radicado = secrets.token_hex(nbytes=randint(10, 12))
        return render_template(
            'registry.html', 
            radicado=radicado.upper(), 
            vehicle_types=vehicle_types, 
            vehicle_brands=vehicle_brands,
            vehicle_models_years=vehicle_models_years
        )


def registry_transaction(form):
    write = write_transaction(iota_api, iota_address, form)
    create = create_transaction(form)
    if write and create:
        set_transaction_log(form['radicado'], write)
    return redirect('/explorer')



@Home.route('/explorer', methods=['GET'])
def explorer():
    tangle_log_list = get_transaction_log()
    return render_template('explorer.html', transaction_list=tangle_log_list)


@Home.route('/detail/<query_type>', methods=['POST'])
def detail(query_type):
    form = request.form.to_dict()
    transaction = None
    tail_transaction_hash = None

    if form:
        if query_type == 'tangle':
            tail_transaction_hash = form['tail_transaction_hash']
            transaction = read_transaction(iota_api, tail_transaction_hash)
        elif query_type == 'db':
            transaction = select_transaction(form['radicado'])
    
    return render_template('detail.html', transaction=transaction, tail_transaction_hash=tail_transaction_hash)
