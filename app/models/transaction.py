from app import DB as db

class Transaction(db.Model):

    radicado = db.Column(db.String(80), unique=True, nullable=False, primary_key=True)
    placa = db.Column(db.String(10), unique=False, nullable=False)
    vehiculo = db.Column(db.String(150), unique=False, nullable=False)
    marca = db.Column(db.String(150), unique=False, nullable=False)
    modelo = db.Column(db.String(150), unique=False, nullable=False)
    cedula = db.Column(db.String(15), unique=False, nullable=False)
    nombre = db.Column(db.String(200), unique=False, nullable=False)
    fecha_nacimiento = db.Column(db.String(20), unique=False, nullable=False)
    seguro = db.Column(db.String(10), unique=False, nullable=False)
