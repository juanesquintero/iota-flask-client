from app.models import Transaction
from app.utils.mixins import insert_row_from_form, update_record
from app.utils.constants import MESSAGES
from flask import flash


def create_transaction(transaction):
    # Create and save user
    saved = insert_row_from_form(Transaction, transaction)
    if saved:
        flash(MESSAGES['DB_SUCCSESS_CREATION'], 'success')
        return True
    else:
        flash(MESSAGES['DB_FAILED_CREATION'], 'danger')
        return False

def select_transaction(radicado):
    transaction = Transaction().query.filter_by(radicado=radicado).first()
    if transaction:
        flash(MESSAGES['DB_SUCCSESS_SELECTION'], 'success')
        return transaction
    else:
        flash(MESSAGES['DB_FAILED_SELECTION'], 'danger')
        return None
    