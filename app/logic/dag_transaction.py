import os
import csv
import pandas
import json
import logging

from flask import flash
from iota import Iota, ProposedTransaction, Address, Tag, TryteString
from app.utils.constants import MESSAGES


error_logger = logging.getLogger('error_logger')

def read_transaction(api, tail_transaction_hash):
    try:
        # Get the transaction objects in the bundle
        bundle = api.get_bundles(tail_transaction_hash)
        # Print the message that's in the first transaction's `signatureMesageFragment` field
        message = bundle['bundles'][0].tail_transaction.signature_message_fragment
        # Convert the message from trytes to ASCII characters
        text = message.decode()
        text_object = json.loads(text)
        flash(MESSAGES['TANGLE_SUCCESS_READING'], 'success')
        return text_object
    except Exception as e:
        error_logger.error('EXCEPTION: '+str(e), exc_info=True)
        flash(MESSAGES['TANGLE_FAIL_READING'], 'danger')
        return False


def write_transaction(api, address, msg):
    try:
        transaction_object = json.dumps(dict(msg))
        message = TryteString.from_unicode(transaction_object)
        tx = ProposedTransaction(
            address=Address(address),
            message=message,
            value=0
        )
        result = api.send_transfer(transfers=[tx])
        tail_transaction_hash = result['bundle'].tail_transaction.hash
        flash(MESSAGES['TANGLE_SUCCESS_WRITING'], 'success')
        return tail_transaction_hash
    except Exception as e:
        error_logger.error('EXCEPTION: '+str(e), exc_info=True)
        flash(MESSAGES['TANGLE_FAIL_WRITING'], 'danger')
        return False


def set_transaction_log(id_register, tail_transaction_hash):
    path_file = os.getcwd()+'/tangle.csv'
    with open(path_file, 'a') as csvfile:
        fieldnames = ['radicado', 'tail_transaction_hash']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writerow({
            'radicado': id_register,
            'tail_transaction_hash': tail_transaction_hash
        })


def get_transaction_log():
    path_file = os.getcwd()+'/tangle.csv'
    tangle_log_pandas = pandas.read_csv(path_file)
    tangle_log_jsons = json.loads(tangle_log_pandas.to_json(orient='records'))
    return tangle_log_jsons
