
MESSAGES = dict(
    TANGLE_SUCCESS_WRITING='Registro guardado en el IOTA Tangle! :)',
    TANGLE_FAIL_WRITING='Error escribiendo el registro en el IOTA Tangle',
    
    TANGLE_SUCCESS_READING='Registro Obtenido de IOTA Tangle :)',
    TANGLE_FAIL_READING='Error leyendo el registro en el IOTA Tangle',
    
    DB_SUCCSESS_CREATION='Registro creado en la base de datos!',
    DB_FAILED_CREATION='Error creando el registro en la base de datos!',

    DB_SUCCSESS_SELECTION='Registro obtenido de la base de datos :)',
    DB_FAILED_SELECTION='No se pudo recuperar el regidtro de la base de datos!',
)


vehicle_types = [
'Carro',
'Moto',
'Camioneta',
'Camion',
'Bicicleta',
'--Otro--'
]

vehicle_brands = [
'Chevrolet',
'Mazda',
'Renault',
'Ford',
'BMW',
'Mercedes Benz',
'--Otro--'
]
vehicle_models_years = [2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, '--Otro--']