# Add your own utility classes and functions here.
import logging

from .constants import MESSAGES
from app import DB

error_logger = logging.getLogger('error_logger')


def insert_row_from_form(db_model, form):
    try:
        obj = db_model(**form)
        DB.session.add(obj)
        DB.session.commit()
        return obj
    except Exception as e:
        error_logger.error('EXCEPTION: '+str(e), exc_info=True)
        return False


def update_record(obj, field_name, new_value):
    try:
        current_value = getattr(obj, field_name)
        setattr(obj, field_name, new_value)
        DB.session.commit()
        return obj
    except Exception as e:
        error_logger.error('EXCEPTION: '+str(e), exc_info=True)
        return False
